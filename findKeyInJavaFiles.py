import os
import csv

def search_files(primarykey, folderpath, outputfile):
    with open(outputfile, "w", newline='') as outfile:
        writer = csv.writer(outfile)
        for root, dirs, files in os.walk(folderpath):
            for file in files:
                if file.endswith(".java"):
                    filepath = os.path.join(root, file)
                    with open(filepath, "r") as infile:
                        for line in infile:
                            if primarykey in line:
                                writer.writerow([filepath, line.strip()])

search_files("SpringApplicationBuilder", "c:\2020\wara", "c:\2020\wara\output.csv")
